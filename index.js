const dgram = require("dgram");
const server = dgram.createSocket("udp4");
const socketClient = require("./libs/socket.io/client");
// const socketC = new socketClient();

server.on("error", err => {
  console.log(`server error:\n${err.stack}`);
  server.close();
});

server.on("message", (msg, rinfo) => {
  console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`);

//   console.log(socketC);
  socketClient.emit("udplog", {
    data: msg.toString().trim(),
    info: rinfo
  });
});

server.on("listening", () => {
  const address = server.address();
  console.log(`server listening ${address.address}:${address.port}`);
});

server.bind(9881, '172.31.30.208');

// server.on('connection', (socket) => {
//     socket.on('data', (data) => {
//        console.log(data.toString().trim(), new Date());
//     //    socket.write('return');
//     });
// });

// server.listen(9881,() => {
//     console.log('server listening 9881');
// });
// var server = net.createServer();
// server.on('connection', function(socket) {
