var app = require("express")();
var http = require("http").Server(app);
var io = require("socket.io")(http);

app.get("/", function(req, res) {
  res.send("<h1>Hello world</h1>");
});

io.on("connection", function(socket) {
  console.log("connection");
  socket.on("udplog", data => {
    console.log(data);
  });
});

http.listen(9882, function() {
  console.log("listening on *:9882");
});
